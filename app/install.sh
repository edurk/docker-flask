#!/bin/bash

PYTHON_VIRTUAL_ENV='env'

deactivate 2>/dev/null

rm -rf "$PYTHON_VIRTUAL_ENV"

python3.5 -m venv "$PYTHON_VIRTUAL_ENV"

. "$PYTHON_VIRTUAL_ENV"/bin/activate
# echo $(which python)

pip install --upgrade pip

pip install -r requirements.txt

echo "import sys; import os; sys.path.append(os.path.abspath(sys.executable+'/../../..'))" > "$PYTHON_VIRTUAL_ENV"/lib/python3.5/site-packages/app-root-path.pth

mkdir -p log
touch log/rest.log