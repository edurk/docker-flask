# -*- coding: utf-8 -*-
from app.src import application


if __name__ == '__main__':
    application.run(
        debug=True,
        host="172.17.0.3",
        port=int("80")
    )
