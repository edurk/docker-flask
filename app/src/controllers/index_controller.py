#!env/bin/python3.5
from flask_classy import route
from app.src.controllers.controller import Controller


class IndexController(Controller):
    def __init__(self):
        pass

    @route('/', methods=['GET'])
    def index(self):
        return 'welcome to docker-flask v 1.0'
