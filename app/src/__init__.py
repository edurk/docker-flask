from flask import Flask
from . import controllers

application = Flask(__name__)

controllers.IndexController.register(application, route_base='/')


@application.errorhandler(404)
def not_found(error):
    return 'not found'
