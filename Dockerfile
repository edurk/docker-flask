
FROM ubuntu:16.04

MAINTAINER <edu.kmxo@gmail.com>

#run echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN perl -p -i.orig -e 's/archive.ubuntu.com/mirrors.aliyun.com\/ubuntu/' /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y build-essential git curl sudo nano vim
RUN apt-get install -y python3 python3-dev python3-setuptools python3-pip python3-venv
RUN apt-get install -y nginx supervisor

# install uwsgi now because it takes a little while
RUN pip3 install uwsgi

# install nginx
RUN apt-get install -y software-properties-common python-software-properties
RUN apt-get update
RUN add-apt-repository -y ppa:nginx/stable

# install our code
RUN mkdir /var/www/httpdocs/
ADD . /var/www/httpdocs/

# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /var/www/httpdocs/nginx-app.conf /etc/nginx/sites-enabled/
RUN ln -s /var/www/httpdocs/supervisor-app.conf /etc/supervisor/conf.d/

# run pip install
RUN pip3 install --upgrade pip
RUN pip3 install -r /var/www/httpdocs/app/requirements.txt


EXPOSE 80
CMD ["supervisord", "-n"]
